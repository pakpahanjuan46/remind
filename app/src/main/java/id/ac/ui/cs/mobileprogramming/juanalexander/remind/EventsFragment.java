package id.ac.ui.cs.mobileprogramming.juanalexander.remind;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.List;

import static id.ac.ui.cs.mobileprogramming.juanalexander.remind.MainActivity.user;

public class EventsFragment extends Fragment {

    public EventsFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final EventDatabase db = Room.databaseBuilder(getActivity().getApplicationContext(), EventDatabase.class, "event_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();

        View rootView = inflater.inflate(R.layout.fragment_events, container, false);

        ListView listOfEvent = (ListView) rootView.findViewById(R.id.lv_list_of_event);

        final List<EventDetail> events = db.eventDao().getAll(user);

        EventAdapter adapter = new EventAdapter(getActivity(), events);

        listOfEvent.setAdapter(adapter);

        listOfEvent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                db.eventDao().delete(events.get(i));
                EventsFragment refresh = new EventsFragment();
                getActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, refresh, "findThisFragment")
                        .addToBackStack(null)
                        .commit();
            }
        });

        return rootView;
    }
}
