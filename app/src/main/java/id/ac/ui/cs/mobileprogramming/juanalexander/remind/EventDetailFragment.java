package id.ac.ui.cs.mobileprogramming.juanalexander.remind;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class EventDetailFragment extends Fragment {
    private String label;
    private String time;
    private String date;

    public EventDetailFragment(EventDetail event) {
        this.label = event.getLabel();
        this.time = event.getTime();
        this.date = event.getDate();
    }

    public EventDetailFragment(int contentLayoutId, EventDetail event) {
        super(contentLayoutId);
        this.label = event.getLabel();
        this.time = event.getTime();
        this.date = event.getDate();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_event_detail, container, false);

        TextView tvLabel = (TextView) rootView.findViewById(R.id.tv_label);
        TextView tvTime = (TextView) rootView.findViewById(R.id.tv_saved_time);
        TextView tvDate = (TextView) rootView.findViewById(R.id.tv_date);

        tvLabel.setText(label);
        tvTime.setText(time);
        tvDate.setText(date);

        return rootView;
    }
}

