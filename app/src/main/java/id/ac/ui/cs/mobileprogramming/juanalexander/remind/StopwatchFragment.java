package id.ac.ui.cs.mobileprogramming.juanalexander.remind;

import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Locale;


public class StopwatchFragment extends Fragment {

    private TextView textView ;
    private Button start, pause, reset;
    private long MillisecondTime, StartTime, TimeBuff, UpdateTime = 0L ;
    private Handler handler;
    private int Seconds, Minutes, MilliSeconds ;

    public StopwatchFragment() {
    }

    static {
        System.loadLibrary("native-lib");
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.fragment_stopwatch, container, false);


        textView = (TextView)rootView.findViewById(R.id.textView);
        start = (Button)rootView.findViewById(R.id.button);
        pause = (Button)rootView.findViewById(R.id.button2);
        reset = (Button)rootView.findViewById(R.id.button3);

        handler = new Handler() ;

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StartTime = SystemClock.uptimeMillis();
                handler.postDelayed(runnable, 0);

                reset.setEnabled(false);

            }
        });

        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                TimeBuff += MillisecondTime;

                handler.removeCallbacks(runnable);

                reset.setEnabled(true);

            }
        });

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                MillisecondTime = 0L ;
                StartTime = 0L ;
                TimeBuff = 0L ;
                UpdateTime = 0L ;
                Seconds = 0 ;
                Minutes = 0 ;
                MilliSeconds = 0 ;
                String startTime = "00:00:00";

                textView.setText(startTime);
            }
        });



        return rootView;

    }

    /*
    A Runnable object which when run, will do the stopwatch main thing
     */
    public Runnable runnable = new Runnable() {

        public void run() {
            MillisecondTime = substractFromJNI(SystemClock.uptimeMillis() , StartTime);
            UpdateTime = addFromJNI(TimeBuff, MillisecondTime);
            Seconds = (int) divideFromJNI(UpdateTime , 1000);
            Minutes = divideFromJNI(Seconds , 60);
            Seconds = moduloFromJNI(Seconds , 60);
            MilliSeconds = (int) moduloFromJNI(UpdateTime , 1000);

            String str = "" + Minutes + ":" + String.format(Locale.ENGLISH, "%02d", Seconds) + ":" + String.format(Locale.ENGLISH, "%03d", MilliSeconds);

            textView.setText(str);

            handler.postDelayed(this, 0);
        }

    };

    public native int addFromJNI(long x, long y);
    public native int substractFromJNI(long x, long y);
    public native int moduloFromJNI(long x, long y);
    public native int divideFromJNI(long x, long y);
}