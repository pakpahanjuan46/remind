package id.ac.ui.cs.mobileprogramming.juanalexander.remind;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {EventDetail.class}, version = 1, exportSchema = false)
public abstract class EventDatabase extends RoomDatabase {
    public abstract EventDao eventDao();
}
