package id.ac.ui.cs.mobileprogramming.juanalexander.remind;


import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;

import id.ac.ui.cs.mobileprogramming.juanalexander.remind.EventDatabase;

import id.ac.ui.cs.mobileprogramming.juanalexander.remind.R;

import static android.app.Activity.RESULT_OK;
import static id.ac.ui.cs.mobileprogramming.juanalexander.remind.MainActivity.user;

public class ProfileFragment extends Fragment {

    private EditText networkName;
    private EditText networkPassword;
    private Button connectButton;


    private EditText usernameText;
    private Button loginButton;

    private Button uploadButton;
    private ImageView mImageView;

    private static final int IMAGE_PICK_CODE = 1000;

    public ProfileFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final EventDatabase db = Room.databaseBuilder(getActivity().getApplicationContext(), EventDatabase.class, "event_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();

        final View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        networkName = (EditText) rootView.findViewById(R.id.et_ssid);
        networkPassword = (EditText) rootView.findViewById(R.id.et_pass);
        connectButton = (Button) rootView.findViewById(R.id.bt_connect);

        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager inputManager = (InputMethodManager)
                        getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

                inputManager.hideSoftInputFromWindow(
                        (null == getActivity().getCurrentFocus()) ? null :
                                getActivity().getCurrentFocus().getWindowToken(),
                        InputMethodManager.HIDE_NOT_ALWAYS);


                try {
                    connectToWifi(networkName.getText().toString(), networkPassword.getText().toString());
                    Toast.makeText(getActivity().getApplicationContext(), "you have connected to WiFi", Toast.LENGTH_LONG).show();

                } catch (Exception e) {
                    Toast.makeText(getContext(), "cannot connect to WiFi", Toast.LENGTH_LONG).show();
                }

            }
        });


        usernameText = (EditText) rootView.findViewById(R.id.et_username);
        loginButton = (Button) rootView.findViewById(R.id.bt_login);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConnectivityManager connManager = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

                if (mWifi.isConnected()) {
                    String username = usernameText.getText().toString();
                    user = username;
                    Toast.makeText(getContext(), "You are logged in as " + username, Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getContext(), "Connect to internet first before logging in", Toast.LENGTH_LONG).show();
                }
            }

        });

        mImageView = (ImageView) rootView.findViewById((R.id.iv_gallery));
        uploadButton = (Button) rootView.findViewById((R.id.bt_gallery));
        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PermissionListener permissionListener = new PermissionListener() {
                    @Override
                    public void onPermissionGranted() {
                        Toast.makeText(getContext(), "Access granted", Toast.LENGTH_LONG).show();
//                        startActivityForResult(new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.INTERNAL_CONTENT_URI), ;
                        pickImage();

                    }

                    @Override
                    public void onPermissionDenied(List<String> deniedPermissions) {
                        Toast.makeText(getContext(), "Access denied", Toast.LENGTH_LONG).show();
                    }
                };

                TedPermission.with(getContext())
                        .setPermissionListener(permissionListener)
                        .setPermissions(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .check();
            }
        });

        return rootView;

    }
    public void connectToWifi(String name, String password){
        try{
            WifiManager wifiManager = (WifiManager) super.getContext().getSystemService(android.content.Context.WIFI_SERVICE);
            WifiConfiguration wc = new WifiConfiguration();
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            wc.SSID = "\"" + name + "\"";
            wc.preSharedKey = "\"" + password + "\"";;
            wc.status = WifiConfiguration.Status.ENABLED;
            wc.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
            wc.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);

            wifiManager.setWifiEnabled(true);
            int netId = wifiManager.addNetwork(wc);
            if (netId == -1) {
                netId = getExistingNetworkId(wc.SSID);
            }
            wifiManager.disconnect();
            wifiManager.enableNetwork(netId, true);
            wifiManager.reconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private int getExistingNetworkId(String SSID) {
        WifiManager wifiManager = (WifiManager) super.getContext().getSystemService(Context.WIFI_SERVICE);
        List<WifiConfiguration> configuredNetworks = wifiManager.getConfiguredNetworks();
        if (configuredNetworks != null) {
            for (WifiConfiguration existingConfig : configuredNetworks) {
                if (existingConfig.SSID.equals(SSID)) {
                    return existingConfig.networkId;
                }
            }
        }
        return -1;
    }

    private void pickImage(){
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE) {
            mImageView.setImageURI(data.getData());
        }
    }
}