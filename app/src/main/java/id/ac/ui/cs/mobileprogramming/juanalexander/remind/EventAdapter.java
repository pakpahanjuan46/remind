package id.ac.ui.cs.mobileprogramming.juanalexander.remind;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.List;

public class EventAdapter extends ArrayAdapter<EventDetail> {
    private Activity context;
    private List<EventDetail> listOfEvent;

    public EventAdapter(Activity context, List<EventDetail> listOfEvent) {
        super(context, R.layout.event_adapter, listOfEvent);
        this.context = context;
        this.listOfEvent = listOfEvent;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();

        @SuppressLint("ViewHolder") View view = inflater.inflate(R.layout.event_adapter, null, true);

        TextView tvLabel = (TextView) view.findViewById(R.id.tv_label);
        TextView tvTime = (TextView) view.findViewById(R.id.tv_saved_time);
        TextView tvDate = (TextView) view.findViewById(R.id.tv_date);

        EventDetail event = listOfEvent.get(position);

        tvLabel.setText(event.getUsername()+" -- "+event.getLabel());
        tvTime.setText(event.getTime());
        tvDate.setText(event.getDate());

        return view;
    }
}
