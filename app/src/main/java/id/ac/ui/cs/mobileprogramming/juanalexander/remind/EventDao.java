package id.ac.ui.cs.mobileprogramming.juanalexander.remind;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface EventDao {

    @Insert
    void insert(EventDetail event);

    @Query("SELECT * FROM event_table WHERE user_name LIKE :username")
    List<EventDetail> getAll(String username);

    @Delete
    void delete(EventDetail event);
}
