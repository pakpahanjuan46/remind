package id.ac.ui.cs.mobileprogramming.juanalexander.remind;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "event_table")
public class EventDetail {
    @PrimaryKey(autoGenerate = true)
    public int uid;

    @ColumnInfo(name = "date")
    private String date;

    @ColumnInfo(name = "time")
    private String time;

    @ColumnInfo(name = "label")
    private String label;

    @ColumnInfo(name = "user_name")
    private String username;

    public EventDetail(String date, String time, String label, String username){
        this.date = date;
        this.time = time;
        this.label = label;
        this.username = username;
    }

    public String getTime(){
        return this.time;
    }

    public String getLabel(){
        return this.label;
    }

    public String getDate() {
        return this.date;
    }

    public String getUsername() {
        return this.username;
    }

}

