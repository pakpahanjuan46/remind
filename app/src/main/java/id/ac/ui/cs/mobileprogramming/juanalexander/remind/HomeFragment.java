package id.ac.ui.cs.mobileprogramming.juanalexander.remind;


import android.app.AlarmManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import static android.content.Context.ALARM_SERVICE;


import static id.ac.ui.cs.mobileprogramming.juanalexander.remind.MainActivity.user;


public class HomeFragment extends Fragment {

    private TextView myTime;
    private String date;
    private static int hours;
    private static int minutes;
    int NotID = 1;

    public HomeFragment() {
        this.date = new String();
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final EventDatabase db = Room.databaseBuilder(getActivity().getApplicationContext(), EventDatabase.class, "event_table").allowMainThreadQueries().addCallback(new RoomDatabase.Callback() {
            @Override
            public void onCreate(@NonNull SupportSQLiteDatabase db) {
                super.onCreate(db);
            }
        }).build();

        final View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        CalendarView calendar = (CalendarView) rootView.findViewById(R.id.cv_home);
        final TextView myDate = (TextView) rootView.findViewById(R.id.tv_current_date);
        Button timePicker = (Button) rootView.findViewById(R.id.bt_time_picker);
        myTime = (TextView) rootView.findViewById(R.id.tv_time);
        final EditText label = (EditText) rootView.findViewById(R.id.et_label);
        Button createEventButton = (Button) rootView.findViewById(R.id.bt_create_event);

        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                date = i2 + "/" +  (i1+1) + "/" + i;
                myDate.setText(date);
            }
        });

        timePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimeDialog();
            }
        });
        createEventButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String newDate = myDate.getText().toString();
                String newTime = myTime.getText().toString();
                String newLabel = label.getText().toString();

                if (newTime.equals("Pick your time")||newTime.equals("Pilih waktu anda")||
                        newDate.equals("Mark your date")||newDate.equals("Pilih tanggal anda")){
                    Toast.makeText(getContext(), "Pick your date and time first!", Toast.LENGTH_LONG).show();
                }
                else if (newLabel.matches("")){
                    Toast.makeText(getContext(), "Input event name first", Toast.LENGTH_LONG).show();
                }
                else if (user.equals("")){
                    Toast.makeText(getContext(), "You need to login first", Toast.LENGTH_LONG).show();
                }
                else {
                    String eventDateTime = newDate + " - " + newTime;
                    EventDetail newEvent = new EventDetail(newDate, newTime, newLabel, user);
                    Toast.makeText(getContext(), ("event object created"), Toast.LENGTH_LONG).show();
                    db.eventDao().insert(newEvent);

                    notlater(newLabel, hours, minutes);
                    Toast.makeText(getContext(), (user+", "+ newLabel +" saved : " + eventDateTime), Toast.LENGTH_LONG).show();
                }
            }
        });


        return rootView;
    }

    private void notlater(String label, int hour, int minute) {
        Intent notificationIntent = new Intent(".MainActivity");
        PendingIntent contentIntent = PendingIntent.getActivity(getContext(), 0, notificationIntent, 0);

        AlarmManager alarmManager = (AlarmManager) getContext().getSystemService(ALARM_SERVICE);

        java.util.Calendar calendar = java.util.Calendar.getInstance();

        //---sets the time for the alarm to trigger in 2 minutes from now---
        calendar.set(java.util.Calendar.HOUR_OF_DAY, hour);
        calendar.set(java.util.Calendar.MINUTE, minute);
        calendar.set(java.util.Calendar.SECOND, 0);


        alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), contentIntent);

        Notification notification = new NotificationCompat.Builder(getContext(), "alarm")
                .setSmallIcon(R.drawable.ic_one)
                .setContentTitle(label)
                .setContentText("Your Event is being held now")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .build();

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

        notificationManager.notify(1, notification);

    }

    private void showTimeDialog() {

        /**
         * Calendar untuk mendapatkan waktu saat ini
         */
        Calendar calendar = Calendar.getInstance();

        /**
         * Initialize TimePicker Dialog
         */
        TimePickerDialog timePickerDialog = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                /**
                 * Method ini dipanggil saat kita selesai memilih waktu di DatePicker
                 */

                hours = hourOfDay;
                minutes = minute;

                String minuteString;
                int length = (int) (Math.log10(minute) + 1);
                if (length==1){
                    minuteString = "0"+ minute;
                }
                else{
                    minuteString = String.valueOf(minute);
                }

                String hourString;
                length = (int) (Math.log10(hourOfDay) + 1);
                if (length==1){
                    hourString = "0"+hourOfDay;
                }
                else{
                    hourString = String.valueOf(hourOfDay);
                }

                myTime.setText(hourString+":"+minuteString);
            }
        },
                /**
                 * Tampilkan jam saat ini ketika TimePicker pertama kali dibuka
                 */
                calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE),

                /**
                 * Cek apakah format waktu menggunakan 24-hour format
                 */
                DateFormat.is24HourFormat(getContext()));

        timePickerDialog.show();
    }

}
