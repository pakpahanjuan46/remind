#include <jni.h>
#include <string>

extern "C"
JNIEXPORT jlong JNICALL
Java_id_ac_ui_cs_mobileprogramming_juanalexander_remind_StopwatchFragment_addFromJNI(JNIEnv *env,
                                                                                  jobject thiz,
                                                                                  jlong x, jlong y) {
    //return a long
    return x + y;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_id_ac_ui_cs_mobileprogramming_juanalexander_remind_StopwatchFragment_substractFromJNI(JNIEnv *env,
                                                                                     jobject thiz,
                                                                                     jlong x, jlong y) {
    //return a long
    return x - y;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_id_ac_ui_cs_mobileprogramming_juanalexander_remind_StopwatchFragment_moduloFromJNI(JNIEnv *env,
                                                                                           jobject thiz,
                                                                                           jlong x, jlong y) {
    //return a long
    return x % y;
}

extern "C"
JNIEXPORT jlong JNICALL
Java_id_ac_ui_cs_mobileprogramming_juanalexander_remind_StopwatchFragment_divideFromJNI(JNIEnv *env,
                                                                                        jobject thiz,
                                                                                        jlong x, jlong y) {
    //return a long
    return x / y;
}
